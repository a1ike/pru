"use strict";

jQuery(function ($) {
  $(".phone").inputmask({
    mask: "+7(999)-999-99-99",
    showMaskOnHover: false
  });
  $(".p-home-view__text_1").hover(function (e) {
    $(".view_1").addClass("view_active");
  }, function (e) {
    $(".view_1").removeClass("view_active");
  });
  $(".view_1").hover(function (e) {
    $(".p-home-view__text_1").addClass("p-home-view__text_active");
  }, function (e) {
    $(".p-home-view__text_1").removeClass("p-home-view__text_active");
  });
  $(".p-home-view__text_2").hover(function (e) {
    $(".view_2").addClass("view_active");
  }, function (e) {
    $(".view_2").removeClass("view_active");
  });
  $(".view_2").hover(function (e) {
    $(".p-home-view__text_2").addClass("p-home-view__text_active");
  }, function (e) {
    $(".p-home-view__text_2").removeClass("p-home-view__text_active");
  });
  $(".p-home-view__text_3").hover(function (e) {
    $(".view_3").addClass("view_active");
  }, function (e) {
    $(".view_3").removeClass("view_active");
  });
  $(".view_3").hover(function (e) {
    $(".p-home-view__text_3").addClass("p-home-view__text_active");
  }, function (e) {
    $(".p-home-view__text_3").removeClass("p-home-view__text_active");
  });
  $(".p-home-view__text_4").hover(function (e) {
    $(".view_4").addClass("view_active");
  }, function (e) {
    $(".view_4").removeClass("view_active");
  });
  $(".view_4").hover(function (e) {
    $(".p-home-view__text_4").addClass("p-home-view__text_active");
  }, function (e) {
    $(".p-home-view__text_4").removeClass("p-home-view__text_active");
  });
  $(".p-home-view__text_5").hover(function (e) {
    $(".view_5").addClass("view_active");
  }, function (e) {
    $(".view_5").removeClass("view_active");
  });
  $(".view_5").hover(function (e) {
    $(".p-home-view__text_5").addClass("p-home-view__text_active");
  }, function (e) {
    $(".p-home-view__text_5").removeClass("p-home-view__text_active");
  });
  $(".p-home-view__text_6").hover(function (e) {
    $(".view_6").addClass("view_active");
  }, function (e) {
    $(".view_6").removeClass("view_active");
  });
  $(".view_6").hover(function (e) {
    $(".p-home-view__text_6").addClass("p-home-view__text_active");
  }, function (e) {
    $(".p-home-view__text_6").removeClass("p-home-view__text_active");
  });
  $(".open-modal").on("click", function (e) {
    $(".p-modal").toggle();
  });
  $(".p-modal__centered").on("click", function (e) {
    if (e.target.className === "p-modal__centered") {
      $(".p-modal").hide();
    }
  });
  $(".p-modal__close").on("click", function (e) {
    $(".p-modal").hide();
  });
  $(".p-header__mob").on("click", function (e) {
    e.preventDefault();
    $(".p-header__nav").slideToggle("fast");
  });
  $(".p-home-seo__more").on("click", function (e) {
    e.preventDefault();

    if ($(this).hasClass("p-home-seo__more_active")) {
      $(this).removeClass("p-home-seo__more_active");
      $(this).html("Читать дальше");
      $(this).prev().removeClass("p-home-seo__content_active");
    } else {
      $(this).addClass("p-home-seo__more_active");
      $(this).html("Скрыть");
      $(this).prev().addClass("p-home-seo__content_active");
    }
  });
  new Swiper(".p-home-projects__cards", {
    pagination: {
      el: ".p-home-projects .swiper-pagination",
      type: "fraction"
    },
    navigation: {
      nextEl: ".p-home-projects .swiper-button-next",
      prevEl: ".p-home-projects .swiper-button-prev"
    },
    loop: true,
    spaceBetween: 100
  });
  new Swiper(".p-home-reviews__cards", {
    pagination: {
      el: ".p-home-reviews .swiper-pagination",
      type: "fraction"
    },
    navigation: {
      nextEl: ".p-home-reviews .swiper-button-next",
      prevEl: ".p-home-reviews .swiper-button-prev"
    },
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      1200: {
        slidesPerView: 2
      }
    }
  });

  if (window.matchMedia("(max-width: 768px)").matches) {
    $(".p-home-reviews__button").detach().insertAfter(".p-home-reviews__cards");
    new Swiper(".p-article__cards", {
      loop: true,
      spaceBetween: 20
    });
    var swiper = new Swiper(".p-home-view__mob", {
      navigation: {
        nextEl: ".p-home-view__mob .swiper-button-next",
        prevEl: ".p-home-view__mob .swiper-button-prev"
      },
      loop: false,
      spaceBetween: 100
    });
    swiper.slideTo(0);
    $(".view_active").removeClass("view_active");
    $(".view_1").addClass("view_active");
    swiper.on("slideChange", function () {
      if (swiper.activeIndex === 0) {
        $(".view_active").removeClass("view_active");
        $(".view_1").addClass("view_active");
      } else if (swiper.activeIndex === 1) {
        $(".view_active").removeClass("view_active");
        $(".view_2").addClass("view_active");
      } else if (swiper.activeIndex === 2) {
        $(".view_active").removeClass("view_active");
        $(".view_3").addClass("view_active");
      } else if (swiper.activeIndex === 3) {
        $(".view_active").removeClass("view_active");
        $(".view_4").addClass("view_active");
      } else if (swiper.activeIndex === 4) {
        $(".view_active").removeClass("view_active");
        $(".view_5").addClass("view_active");
      } else if (swiper.activeIndex === 5) {
        $(".view_active").removeClass("view_active");
        $(".view_6").addClass("view_active");
      }
    });
  }
});
//# sourceMappingURL=main.js.map